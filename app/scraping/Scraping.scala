package scraping

import scala.concurrent.Future
import scala.io.Source

import com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException
import play.api.Play.current
import play.api.libs.functional.syntax._
import play.api.libs.json._
import play.core.StaticApplication

import models._
import models.database.DatabaseManager


/**
 * Created by schukey on 15-02-25.
 * This should port over scraped data from the files created by scraping.py
 * TODO: clean this up
 */
class Scraping {
    case class ScrapeDish(name: String, price: Double, description: Option[String], imagePath: Option[String], tags: List[String])
    case class ScrapeStore(storeName: String, site: Option[String], email: Option[String], phoneNumber: Option[String], location: String)
    case class ScrapeEntry(store: ScrapeStore, dishes: List[ScrapeDish])

    val manager = DatabaseManager

    // accepts a list of String(tags) for one dish
    def processTags(dishId: Long, tags: List[String]): Unit = {
      tags.map(_.toLowerCase).foreach { t =>
        val tid = manager.insertTag(new DTag(name = t))
        manager.addTagToDish(tagId = tid, dishId = dishId)
      }
    }

    def processData(data: ScrapeEntry): Unit = {
        try {
            // TODO: lat/long values are currently random, will have to add it to db manually later
            val r = Restaurant(None,data.store.storeName,data.store.location,1.0,0.9,data.store.site,data.store.phoneNumber)
            if (data.dishes.length > 0) {
              val restId = manager.insertRestaurant(r)
              data.dishes.foreach { d =>
                val dishId = manager.insertDish(new Dish(None, d.name, restId, d.price, d.description, d.imagePath))
                processTags(dishId, d.tags)
              }
            }
            println("success: added dishes to db from: " + r.name)
        } catch {
            case jdbc: MySQLSyntaxErrorException =>
                println(s"MySQL Syntax Error:\n${jdbc.getMessage}")
            case e: Exception =>
                println(s"Exception occurred:\n")
                e.printStackTrace()
        }
    }

    def importData(json: JsValue): Unit = {
        implicit val dishReads: Reads[ScrapeDish] = (
              (JsPath \ "name").read[String] and
              (JsPath \ "price").read[Double] and
              (JsPath \ "description").read[Option[String]] and
              (JsPath \ "imagePath").read[Option[String]] and
              (JsPath \ "tags").read[List[String]]
        )(ScrapeDish.apply _)

        implicit val storeReads: Reads[ScrapeStore] = (
                              (JsPath \ "storeName").read[String] and
                              (JsPath \ "site").read[Option[String]] and
                              (JsPath \ "email").read[Option[String]] and
                              (JsPath \ "phoneNumber").read[Option[String]] and
                              (JsPath \ "location").read[String]
        )(ScrapeStore.apply _)

        implicit val scrapeEntryReads: Reads[ScrapeEntry] = (
                              (JsPath \ "store").read[ScrapeStore] and
                              (JsPath \ "dishes").read[List[ScrapeDish]]
        )(ScrapeEntry.apply _)

        val scrapeEntry: JsResult[ScrapeEntry] = json.validate[ScrapeEntry](scrapeEntryReads)

        scrapeEntry match {
            case s: JsSuccess[ScrapeEntry] => {
                processData(s.get)
            }
            case e: JsError => println("Error: " + e)
        }
        println("finished reading data")
  }
}

/**
 * Should pull down scraping data from S3, clean the data, change the json data into scala models
 * To run this, right click in intellij > select "Run 'TestScraping'"
 */
object TestScraping {
    def main(args: Array[String]): Unit = {
        println("beginning in main!")

        // make sure a static app is started
        new StaticApplication(new java.io.File("."))

        val s = new Scraping()
        val files = new java.io.File("../scraping").listFiles.filter(_.getName.endsWith(".json"))

        // set up db if not already set up
        s.manager.createAll()

        files.foreach { file =>
            val content = Source.fromFile(file).getLines.mkString
            if (!content.equals("-1") && !content.isEmpty){
                println("importing file: " + file)
                val json: JsValue = Json.parse(content)
                s.importData(json)
            } else {
                println("error importing file: " + file)
            }
        }

        // stop the static app
        play.api.Play.stop()
        println("closed app")
    }
}

