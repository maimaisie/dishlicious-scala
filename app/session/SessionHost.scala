package session

import java.awt.image.DataBufferShort

import models.User
import models._
import java.security.SecureRandom

import models.database.{DatabaseManager, SessionHistory}

/**
 * Created by c8gao on 3/11/2015.
 */
object SessionHost {
  //sessionId == -1 is reserved, it's used to indicate whether the sessionId exists
  def generateSession(userId : Long): DSession ={
    DatabaseManager.getSessionIdFromUserId(userId) match {
      case None =>
        // create new session for this user
        val sr : SecureRandom  = new SecureRandom()
        sr.setSeed(1000)
        var sessionId = -1l
        var session: DSession = null
        
        while (sessionId == -1l) {
          sessionId = Math.abs(sr.nextLong)
          session = new DSession(id = sessionId, createdAt = getCurrentTime())
          sessionId = DatabaseManager.insertSession(session) match {
            case -1 => -1l // sessionId already used, generate another one
            case 1 => sessionId // session successfully inserted in DB
          }
        }

        val sessionHistory = new SessionHistory(sessionId=sessionId, userId=userId)
        DatabaseManager.insertSessionHistoryIfNotExist(sessionHistory)
        
        session
        
      case Some(sh) =>
        // user already exists in a session, return the session!
        DatabaseManager.getSession(sh.sessionId).get
    }
  }

  def connectToHost(userId : Long, sessionId: Long ): Unit ={
    val sessionmHistory = new SessionHistory(sessionId=sessionId, userId=userId)
    DatabaseManager.insertSessionHistoryIfNotExist(sessionmHistory)
  }

  def disconnectFromHost(fbId : Long, sessionId: Long ): Unit ={
    DatabaseManager.removeSessionHistory(fbId, sessionId)
  }

  def getUsersFromSessionId(sessionId: Long): List[User] ={
    DatabaseManager.getUsersFromSessionId(sessionId)
  }

  def getCurrentTime(): java.sql.Date ={
     new java.sql.Date(new java.util.Date().getTime)
  }
}

//object Test {
//  def main(args: Array[String]): Unit = {
//    println(Session.getCurrentTime())
//  }
//}
