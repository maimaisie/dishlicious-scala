package controllers

import java.sql.Date
import models.DSession
import models._
import models.database.DatabaseManager
import play.api._
import play.api.http.Writeable
import play.api.libs.json.{Writes, Json}
import play.api.mvc._
import recommendation.RecommendationEngine
import session.SessionHost
import play.core.Router._

object Application extends Controller {

  def index = Action { implicit request =>
    println("index session:", request.session)
    Ok(views.html.index("Dishlicious"))
  }

  def addUserLoc(latitude: String, longitude: String) = Action { implicit request =>
    Ok("Added location to session").withSession(
      request.session + ("latitude" -> latitude) + ("longitude" -> longitude)
    )
  }

  def settings = Action { implicit request =>
    try {
      request.session.get("fb-id").map { fbid =>
        DatabaseManager.getUserFromFbId(fbid.toLong) match {
          case None =>
            NotFound(views.html.notfound(s"USER $fbid"))
          case Some(user) =>
            val manager = DatabaseManager
            val (wl, bl, allTags) = manager.getSortedTagsForUserOnProfilePage(user.id.get)
            Ok(views.html.settings("Preferences", allTags, wl, bl, user.id.get))
        }
      }.getOrElse {
        // TODO: maybe direct to fb login here
        Redirect(routes.Application.index)
      }
    } catch {
      case e: Exception =>
        Redirect(routes.Application.index)
    }
  }

  /*def profile = Action { implicit request =>
    request.session.get("fb-id").map { fbid =>
      Ok(views.html.profile("Profile"))
    }.getOrElse{
      Redirect(routes.Application.index)
    }
  }*/

  def about = Action { implicit request =>
    Ok(views.html.about("About Dishlicious"))
  }

  def host = Action { implicit request =>
    try {
      request.session.get("fb-id").map { fbid =>
        DatabaseManager.getUserFromFbId(fbid.toLong) match {
          case None =>
            NotFound(views.html.notfound(s"USER $fbid"))
          case Some(user) =>
            // returns new session if user not in active session or returns the existing session
            val session = SessionHost.generateSession(user.id.get)
            val sessionId = session.id
            // TODO: change to absolute URL
            val sessionLink: String = "http://www.dishlicious.ca/group?sessionId=" + sessionId
            Ok(views.html.host("Invite other friends", session, sessionLink, false)).withSession(
              request.session + ("dsession-id" -> session.id.toString)
            )
        }
      }.getOrElse {
        // TODO: maybe direct to fb login here
        Redirect(routes.Application.index)
      }
    } catch {
      case e: Exception =>
        Redirect(routes.Application.index)
    }
  }

  def session(sessionId: Option[String]) = Action { implicit request =>
    var ret = NotFound(views.html.notfound(s"PAGE"))
    try {
      request.session.get("fb-id").map { fbid =>
        DatabaseManager.getUserFromFbId(fbid.toLong) match {
          case None =>
            NotFound(views.html.notfound(s"USER $fbid"))
          case Some(user) =>
            sessionId match {
              case Some(id) =>
                val session = DatabaseManager.getSession(id.toLong).getOrElse {
                  throw new RuntimeException("Session id is invalid/not found")
                }

                DatabaseManager.getActiveSessionForUser(user.id.get).map { existingSessionId =>
                  if (existingSessionId != id.toLong) {
                    // Ask if user wants to leave the previous group
                    val sessionLink: String = "http://www.dishlicious.ca/group?sessionId=" + id
                    ret = Ok(views.html.host("Invite other friends", session, sessionLink, true))
                  } else {
                    val sessionLink: String = "http://www.dishlicious.ca/group?sessionId=" + existingSessionId
                    ret = Ok(views.html.host("Invite other friends", session, sessionLink, false))
                  }
                }.getOrElse {
                  // add user to the session
                  SessionHost.connectToHost(user.id.get, session.id)

                  // TODO: change to absolute URL
                  val sessionLink: String = "http://www.dishlicious.ca/group?sessionId=" + session.id
                  ret = Ok(views.html.host("Invite other friends", session, sessionLink, false)).withSession(
                    request.session + ("dsession-id" -> session.id.toString)
                  )
                }

                ret

              case None =>
                NotFound(views.html.notfound(s"SESSION $sessionId"))
            }
        }
      }.getOrElse {
        // TODO: maybe direct to fb login here
        Redirect(routes.Application.index)
      }
    } catch {
      case e: Exception =>
        NotFound(views.html.notfound(s"PAGE"))
    }
  }

  def recommendations(sessionId: String) = Action { implicit request =>
    try {
      request.session.get("fb-id").map { fbid =>
        DatabaseManager.getSession(sessionId.toLong) match {
          case None =>
            // sessionId not valid!
            NotFound(views.html.notfound(s"SESSION ID $sessionId"))
          case Some(session) =>
            val (wlOfAllUsers, blOfAllUsers) = DatabaseManager.getPreferencesForAllUserInSession(session.id)
            val engine = request.session.get("latitude").map { lat =>
              request.session.get("longitude").map { long =>
                new RecommendationEngine(wlOfAllUsers, blOfAllUsers, (lat.toDouble, long.toDouble))
              }.getOrElse(new RecommendationEngine(wlOfAllUsers, blOfAllUsers))
            }.getOrElse(new RecommendationEngine(wlOfAllUsers, blOfAllUsers))

            val result = engine.recommendDishes(false).map(k => (DatabaseManager.getRestaurant(k._1).get, k._2))

            Ok(views.html.restaurants("Restaurants", result))
        }
      }.getOrElse {
        // TODO: maybe direct to fb login here
        Redirect(routes.Application.index)
      }
    } catch {
      case e: Exception =>
        // sessionId not valid!
        NotFound(views.html.notfound(s"SESSION ID $sessionId"))
    }
  }

  def javascriptRoutes = Action { implicit request =>
    Ok(
      Routes.javascriptRouter("jsRoutes")(
        controllers.routes.javascript.Application.getNext30Tags,
        controllers.routes.javascript.Application.saveUser,
        controllers.routes.javascript.Application.savePreferences,
        controllers.routes.javascript.Application.logout,
        controllers.routes.javascript.Application.leaveSession,
        controllers.routes.javascript.Application.addUserLoc,
        controllers.routes.javascript.Application.getMemberInSession
      )
    ).as("text/javascript")
  }

  def getMemberInSession = Action { implicit request =>
    request.session.get("dsession-id").map { sessionId =>
      Ok(Json.toJson(DatabaseManager.getUsersFromSessionId(sessionId.toLong).map(_.username)))
    }.getOrElse {
      Ok("")
    }
  }

  def leaveSession = Action { implicit request =>
    request.session.get("fb-id").map { fbid =>
      request.session.get("dsession-id").map { sessionId =>
        SessionHost.disconnectFromHost(fbid.toLong, sessionId.toLong)
        Ok(s"User $fbid left group $sessionId").withSession(
          request.session - "dsession-id"
        )
      }.getOrElse {
        // TODO: maybe direct to fb login here
        Redirect(routes.Application.index)
      }
    }.getOrElse {
      // TODO: maybe direct to fb login here
      Redirect(routes.Application.index)
    }
  }

  def getNext30Tags(ids: List[Long]) = Action {
    Ok(Json.toJson(DatabaseManager.getNext30TagsNotIn(ids)))
  }

  def savePreferences(userId: Long, wl: List[Long], bl: List[Long], wlToRemove: List[Long], blToRemove: List[Long]) = Action {
    val wls = wl.map(w => new WhiteListItem(userId = userId, tagId = w))
    val bls = bl.map(w => new BlackListItem(userId = userId, tagId = w))

    DatabaseManager.insertPreferences(wls, bls)
    DatabaseManager.deletePreferences(userId, wlToRemove, blToRemove)

    Ok("Saved user preferences")
  }

  implicit val DTagWrites = new Writes[DTag] {
    def writes(t: DTag) = Json.obj(
      "id" -> t.id,
      "name" -> t.name
    )
  }

  /**
    * Users
    */

  def saveUser(fbId: Long, name: String) = Action { implicit request =>
    DatabaseManager.insertIfNotExist(new User(None, name, None, fbId)) match {
      case true => Ok("true").withSession(
        request.session + ("fb-id" -> fbId.toString)
      )
      case _ => Ok("false").withSession(
        request.session + ("fb-id" -> fbId.toString)
      )
    }
  }

  def logout() = Action { implicit request =>
    println("removing session:", request.session)
    request.session.get("fb-id").map { fbId =>
        Ok("true").withNewSession
    }.getOrElse(
        Ok("false")
    )
  }
}
