package models

import models.database.DatabaseManager
import play.api.db.slick.Database

/**
 * Created by maiwang on 2015-03-08.
 */
case class User(id: Option[Long] = None,
                username: String,
                password: Option[String] = None,
                facebookId: Long)
