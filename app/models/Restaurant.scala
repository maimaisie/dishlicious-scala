package models

import models.database.DatabaseManager

/**
 * Created by maiwang on 2015-03-05.
 */
case class Restaurant(id: Option[Long] = None,
                      name: String,
                      address: String,
                      lat: Double,
                      long: Double,
                      website: Option[String] = None,
                      tel: Option[String] = None)
{

  def getDishes: List[Dish] = {
    id match {
      case None =>
        println(s"Getting dishes from Restaurant $name with no id")
        List.empty[Dish]
      case Some(rid) =>
        DatabaseManager.getDishesFromRestaurant(rid)
    }
  }

  def getGeocode: Option[(Double, Double)] = {
    id match {
      case None =>
        println(s"Getting geocode from Restaurant $name with no id")
        None
      case Some(rid) =>
        DatabaseManager.getRestaurantGeocode(rid)
    }
  }
}
