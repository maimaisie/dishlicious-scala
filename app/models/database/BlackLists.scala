package models.database

//import scala.slick.driver.MySQLDriver.simple._

import models.BlackListItem
import play.api.db.slick.Config.driver.simple._

/**
 * Created by maiwang on 2015-03-03.
 */

class BlackLists(tag: Tag) extends Table[BlackListItem](tag, "blacklists") {
  def id = column[Long]("bid", O.PrimaryKey, O.AutoInc)
  def userId = column[Long]("user_id")
  def tagId = column[Long]("tag_id")
  def * = (id.?, userId, tagId) <> (BlackListItem.tupled, BlackListItem.unapply)

  def userFk = foreignKey("bl_user_fk", userId, Tables.Users)(_.id)
  def tagFk = foreignKey("bl_tag_fk", tagId, Tables.Tags)(_.id)
}