package models.database

//import scala.slick.driver.MySQLDriver.simple._

import models.Restaurant
import play.api.db.slick.Config.driver.simple._


/**
 * Created by maiwang on 2015-03-03.
 */

class Restaurants(tag: Tag) extends Table[Restaurant](tag, "restaurants") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name")
  def address = column[String]("address")
  def latitude = column[Double]("latitude")
  def longitude = column[Double]("longitude")
  def website = column[Option[String]]("web")
  def tel = column[Option[String]]("tel")
  def * = (id.?, name, address, latitude, longitude, website, tel) <> (Restaurant.tupled, Restaurant.unapply)
}
