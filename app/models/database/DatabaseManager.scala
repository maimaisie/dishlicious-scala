package models.database

import models._
import models.DSession
import play.api.db.slick.DB

import scala.math._

//import scala.slick.driver.MySQLDriver.simple._
import play.api.db.slick.Config.driver.simple._
import play.api.Play.current

/**
 * A models.database manager that communicates with the MySql models.database.
 *
 * Created by maiwang on 2015-02-04.
 */


object DatabaseManager {

  /* Create all the tables defined in Tables.scala */
  def createAll(): Unit = {
    DB.withSession {
      implicit session =>
        // Create the tables, including primary and foreign keys
        (Tables.Restaurants.ddl ++
          Tables.Sessions.ddl ++
          Tables.Users.ddl ++
          Tables.Comments.ddl ++
          Tables.Dishes.ddl ++
          Tables.SessionHistories.ddl ++
          Tables.Tags.ddl ++
          Tables.TagLinks.ddl ++
          Tables.WhiteLists.ddl ++
          Tables.BlackLists.ddl).create
    }
  }

  /* Remove all the tables defined in Tables.scala */
  def dropAll(): Unit = {
    //Database.forURL(url = url, driver = driver, user = user, password = password).withSession {
    DB.withSession {
      implicit session =>
        // Drop all the tables, including primary and foreign keys
        (Tables.TagLinks.ddl ++
          Tables.WhiteLists.ddl ++
          Tables.BlackLists.ddl ++
          Tables.SessionHistories.ddl ++
          Tables.Comments.ddl ++
          Tables.Dishes.ddl ++
          Tables.Users.ddl ++
          Tables.Sessions.ddl ++
          Tables.Restaurants.ddl ++
          Tables.Tags.ddl).drop
    }
  }

  /***************************************
    * Dish related functions             *
    **************************************/

  def getDishesFromRestaurant(id: Long): List[Dish] = {
    DB.withSession {
      implicit session =>
        Tables.Dishes.filter(_.restaurant_id === id).list
    }
  }

  def getDish(dishId: Long): Option[Dish] = {
    DB.withSession {
      implicit session =>
        Tables.Dishes.filter(_.id === dishId).firstOption
    }
  }

  def getDishes(dishIds: List[Long]): List[Dish] = {
    DB.withSession {
      implicit session =>
        dishIds.map { id =>
          Tables.Dishes.filter(_.id === id).first
        }
    }
  }

  // given a list of tags, find all dishes that have at least one tag in the tag list (inlcudes duplicates)
  def getDishesFromTags(tags: List[Long], userLoc: (Double, Double), threshold: Double): List[List[Long]] = {
    DB.withSession {
      implicit session =>
        val query1 = for {
          r <- Tables.Restaurants
        }yield (r.id, r.latitude, r.longitude)

        val rests = query1.list.toList.filter(x => getDistance((x._2,x._3),userLoc)<= threshold ).map(x => x._1)

        tags.map { tagid =>
          val query = for {
            tl <- Tables.TagLinks if (tl.tagId === tagid)
            d <- Tables.Dishes if (d.id === tl.dishId && (d.restaurant_id inSet rests))
          } yield (d.id)

          query.list.toList
        }
    }
  }

  //calculate distance between two locations in meters
  def getDistance(loc1: (Double, Double), loc2: (Double, Double)): Double ={
    val theta = loc1._2 - loc2._2
    var distance = (sin(toRadians(loc1._1)) * sin(toRadians(loc2._1))) + (cos(toRadians(loc1._1)) * cos(toRadians(loc2._1)) * cos(toRadians(theta)))
    //var distance = (sin(loc1._1) * sin(loc2._1)) + (cos(loc1._1) * cos(loc2._1) * cos(theta))
    distance = acos(distance)
    distance = toDegrees(distance)
    distance = distance * 60 * 1.1515 * 1609.344
    distance
  }

  def insertDish(dish: Dish): Long = {
    DB.withSession {
      implicit session =>
        (Tables.Dishes returning Tables.Dishes.map(_.id)) += dish
    }
  }

  def deleteDish(id: Long): Unit = {
    DB.withSession {
      implicit session =>
        Tables.Dishes.filter(_.id === id).delete
    }
  }

  /***************************************
    * Restaurant related functions       *
    **************************************/

  // insert a restaurant object into DB, returns id of the newly inserted restaurant
  def insertRestaurant(res: Restaurant): Long = {
    DB.withSession {
      implicit session =>
        (Tables.Restaurants returning Tables.Restaurants.map(_.id)) += res
    }
  }

  def getAllRestaurants(): List[Restaurant] = {
    DB.withSession {
      implicit session =>
        val query = for {
          t <- Tables.Restaurants
        } yield t
        query.list
    }
  }

  def getRestaurant(id: Long): Option[Restaurant] = {
    DB.withSession {
      implicit session =>
        Tables.Restaurants.filter(_.id === id).firstOption
    }
  }

  def deleteRestaurant(id: Long): Unit = {
    DB.withSession {
      implicit session =>
        Tables.Restaurants.filter(_.id === id).delete
    }
  }

  def getRestaurantGeocode(id: Long): Option[(Double, Double)] = {
    DB.withSession {
      implicit session =>
        val query = for {
          r <- Tables.Restaurants if r.id === id
        } yield (r.latitude, r.longitude)

        query.firstOption
    }
  }

  // input: list of restaurant ids
  // output: tuple (restaurant id, lat, long)
  def getGeoCodes(ids: List[Long]): Map[Long, (Double, Double)] = {
    DB.withSession {
      implicit session =>
        ids.map { id =>
          val res = Tables.Restaurants.filter(_.id === id).first
          id -> (res.long, res.lat)
        }.toMap
    }
  }

  /***************************************
    * White/Blacklist related functions  *
    **************************************/

  // insert one item (user-tag relation)
  def insertWhiteListItem(wl: WhiteListItem): Long = {
    DB.withSession {
      implicit session =>
        (Tables.WhiteLists returning Tables.WhiteLists.map(_.id)) += wl
    }
  }

  // insert a list of items
  def insertWhiteListItems(wl: List[WhiteListItem]): Unit = {
    DB.withSession {
      implicit session =>
        wl.foreach { w =>
          Tables.WhiteLists += w
        }
    }
  }

  def deleteWhiteListItem(id: Long): Unit = {
    DB.withSession {
      implicit session =>
        Tables.WhiteLists.filter(_.id === id).delete
    }
  }

  // delete entire whitelist for a user
  def deleteWhiteList(userid: Long): Unit = {
    DB.withSession {
      implicit session =>
        Tables.WhiteLists.filter(_.userId === userid).delete
    }
  }

  def deleteWhiteListItems(userId: Long, ids: List[Long]): Unit = {
    DB.withSession {
      implicit session =>
        ids.foreach { id =>
          Tables.WhiteLists.filter(x => x.userId === userId && x.tagId === id).delete
        }
    }
  }

  // insert one item (user-tag relation)
  def insertBlackListItem(bl: BlackListItem): Long = {
    DB.withSession {
      implicit session =>
        (Tables.BlackLists returning Tables.BlackLists.map(_.id)) += bl
    }
  }

  // insert a list of items
  def insertBlackListItems(bl: List[BlackListItem]): Unit = {
    DB.withSession {
      implicit session =>
        bl.foreach { b =>
          Tables.BlackLists += b
        }
    }
  }

  def deleteBlackListItem(id: Long): Unit = {
    DB.withSession {
      implicit session =>
        Tables.BlackLists.filter(_.id === id).delete
    }
  }

  def deleteBlackListItems(userId: Long, ids: List[Long]): Unit = {
    DB.withSession {
      implicit session =>
        ids.foreach { id =>
          Tables.BlackLists.filter(x => x.userId === userId && x.tagId === id).delete
        }
    }
  }

  // delete entire blacklist for a user
  def deleteBlackList(userid: Long): Unit = {
    DB.withSession {
      implicit session =>
        Tables.BlackLists.filter(_.userId === userid).delete
    }
  }

  def getWhitelistsFromUser(id: Long): List[DTag] = {
    DB.withSession {
      implicit session => {
        val query = for {
          wl <- Tables.WhiteLists if (wl.userId === id)
          t <- Tables.Tags if (t.id === wl.tagId)
        } yield (t)

        query.list.toList
      }
    }
  }

  def getBlacklistsFromUser(id: Long): List[DTag] = {
    DB.withSession {
      implicit session => {
        val query = for {
          bl <- Tables.BlackLists if (bl.userId === id)
          t <- Tables.Tags if (t.id === bl.tagId)
        } yield (t)

        query.list.toList
      }
    }
  }

  def insertPreferences(wl: List[WhiteListItem], bl: List[BlackListItem]): Unit = {
    DB.withSession {
      implicit session =>
        wl.foreach { w =>
          Tables.WhiteLists += w
        }

        bl.foreach { b =>
          Tables.BlackLists += b
        }
    }
  }

  def deletePreferences(userId: Long, wl: List[Long], bl: List[Long]): Unit = {
    DB.withSession {
      implicit session =>
        wl.foreach { id =>
          Tables.WhiteLists.filter(x => x.userId === userId && x.tagId === id).delete
        }

        bl.foreach { id =>
          Tables.BlackLists.filter(x => x.userId === userId && x.tagId === id).delete
        }
    }
  }

  /***************************************
    * USER related functions       *
    **************************************/

  def insertUser(u: User): Long = {
    DB.withSession {
      implicit session =>
        (Tables.Users returning Tables.Users.map(_.id)) += u
    }
  }

  def insertIfNotExist(u: User): Boolean = {
    DB.withSession {
      implicit session =>
        Tables.Users.filter(_.facebookId === u.facebookId).firstOption match {
          case None =>
            (Tables.Users) += u
            true
          case Some(t) =>
            false
        }
    }
  }
  def deleteUser(id: Long): Unit = {
    DB.withSession {
      implicit session =>
        Tables.Users.filter(_.id === id).delete
    }
  }

  def getUser(id: Long): Option[User] = {
    DB.withSession {
      implicit session =>
        Tables.Users.filter(_.id === id).firstOption
    }
  }

  def getUserFromFbId(fbid: Long): Option[User] = {
    DB.withSession {
      implicit session =>
        Tables.Users.filter(_.facebookId === fbid).firstOption
    }
  }


  /***************************************
    * Tag related functions       *
    **************************************/

  def getAllTags(): List[DTag] = {
    DB.withSession {
      implicit session =>
        val query = for {
          t <- Tables.Tags
        } yield t
        query.list
    }
  }

  // create a plain tag; no attachment to any dishes
  def insertTag(tag: DTag): Long = {
    DB.withSession {
      implicit session =>
        // check if the tag exists, if not then insert it
        Tables.Tags.filter(_.name === tag.name).firstOption match {
          case None =>
            (Tables.Tags returning Tables.Tags.map(_.id)) += tag
          case Some(t) =>
            t.id.get
        }
    }
  }

  // add a tag to a dish; assume tag already created
  def addTagToDish(tagId: Long, dishId: Long): Unit = {
    DB.withSession {
      implicit session =>
        Tables.TagLinks += Tuple2(dishId, tagId)
    }
  }

  def removeTagFromDish(tagId: Long, dishId: Long): Unit = {
    DB.withSession {
      implicit session =>
        Tables.TagLinks.filter(tl => tl.dishId === dishId && tl.tagId === tagId).delete
    }
  }

  def deleteTag(id: Long): Unit = {
    DB.withSession {
      implicit session =>
        // first remove links with dishes, if any
        Tables.TagLinks.filter(_.tagId === id).delete
        // then remove tag itself
        Tables.Tags.filter(_.id === id).delete
    }
  }

  def getTagsFromDish(dishId: Long): List[DTag] = {
    DB.withSession {
      implicit session =>
        val query = for {
          tl <- Tables.TagLinks if tl.dishId === dishId
          t <- Tables.Tags if tl.tagId === t.id
        } yield t

        query.list
    }
  }

  def getSortedTagsForUserOnProfilePage(userId: Long): (List[DTag], List[DTag], List[DTag]) = {
    DB.withSession {
      implicit session =>
        // get whitelist
        val query1 = for {
          wl <- Tables.WhiteLists if (wl.userId === userId)
          t <- Tables.Tags if (t.id === wl.tagId)
        } yield (t)

        val wl = query1.list.toList

        val query2 = for {
          bl <- Tables.BlackLists if (bl.userId === userId)
          t <- Tables.Tags if (t.id === bl.tagId)
        } yield (t)

        val bl = query2.list.toList

        // calculate number of tags we need to query again
        val n = 30 - wl.size - bl.size

        val query = (for {
          t <- Tables.Tags if !(t.id inSet wl.map(_.id.get)) && !(t.id inSet bl.map(_.id.get))
          tl <- Tables.TagLinks if tl.tagId === t.id
        } yield tl).groupBy(_.tagId).map { case (tagId, group) =>
          (tagId, group.size)
        }.sortBy(_._2.desc).map(_._1)

        val allTagsId = query.list
        val top30 = allTagsId.take(n)

        val top30Tags = top30.map { id =>
          Tables.Tags.filter(_.id === id).first
        }

        (wl, bl, top30Tags)
    }
  }

  def getNext30TagsNotIn(idsToExclude: List[Long]): List[DTag] = {
    DB.withSession {
      implicit session =>
        val query = (for {
          t <- Tables.Tags if !(t.id inSet idsToExclude)
          tl <- Tables.TagLinks if tl.tagId === t.id
        } yield tl).groupBy(_.tagId).map { case (tagId, group) =>
          (tagId, group.size)
        }.sortBy(_._2.desc).map(_._1)

        val allTagsId = query.list
        val top30 = allTagsId.take(30)

        top30.map { id =>
          Tables.Tags.filter(_.id === id).first
        }
    }
  }

  /***************************************
    * Session related functions       *
    **************************************/
  def insertSession(s: DSession): Long = {
    DB.withSession {
      implicit session =>
        Tables.Sessions.filter(_.id === s.id).firstOption match {
          case None =>
            Tables.Sessions += s
            1
          case Some(session) =>
            -1
        }
    }
  }

  def insertSessionHistoryIfNotExist(s: SessionHistory): Unit = {
    DB.withSession {
      implicit session =>
        Tables.SessionHistories.filter(x => x.userId === s.userId && x.sessionId === s.sessionId).firstOption match {
          case None =>
            Tables.SessionHistories += s
          case Some(sh) =>
            // no op
        }
    }
  }

  def removeSessionHistory(fbId : Long, sessionId: Long ): Unit = {
    DB.withSession {
      implicit session =>
        val userId = Tables.Users.filter(_.facebookId === fbId).first.id.get
        val ret = Tables.SessionHistories.filter(x => (x.userId === userId && x.sessionId === sessionId)).delete
    }
  }

  def getSessionIdFromUserId(userId: Long): Option[SessionHistory] = {
    DB.withSession {
      implicit session =>
        Tables.SessionHistories.filter(_.userId === userId).firstOption
    }
  }

  def getUsersFromSessionId(sessionId: Long): List[User] ={
    DB.withSession {
      implicit session => {
        val query = for {
          sh <- Tables.SessionHistories if (sh.sessionId === sessionId)
          u <- Tables.Users if sh.userId === u.id
        } yield (u)

        query.list.toList
      }
    }
  }

  def getPreferencesForAllUserInSession(sessionId: Long): (List[List[Long]], List[List[Long]]) = {
    DB.withSession {
      implicit session => {
        val query = for {
          sh <- Tables.SessionHistories if (sh.sessionId === sessionId)
          u <- Tables.Users if sh.userId === u.id
        } yield (u.id)

        val users = query.list

        val wl = users.map { id =>
          val querywl = for {
            wl <- Tables.WhiteLists if (wl.userId === id)
            t <- Tables.Tags if (t.id === wl.tagId)
          } yield (t.id)

          querywl.list
        }

        val bl = users.map { id =>
          val querybl = for {
            bl <- Tables.BlackLists if (bl.userId === id)
            t <- Tables.Tags if (t.id === bl.tagId)
          } yield (t.id)

          querybl.list
        }

        (wl, bl)
      }
    }
  }

  def getSession(sessionId: Long): Option[DSession] = {
    DB.withSession {
      implicit session => {
        Tables.Sessions.filter(_.id === sessionId).firstOption
      }
    }
  }

  def getActiveSessionForUser(userid: Long): Option[Long] = {
    DB.withSession {
      implicit session => {
        Tables.SessionHistories.filter(_.userId === userid).firstOption match {
          case None => None
          case Some(sh) => Some(sh.sessionId)
        }
      }
    }
  }
}
