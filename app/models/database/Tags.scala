package models.database

//import scala.slick.driver.MySQLDriver.simple._
import play.api.db.slick.Config.driver.simple._
import models.DTag

/**
 * Created by maiwang on 2015-03-03.
 */

class TagLinks(tag: Tag) extends Table[(Long, Long)](tag, "taglinks") {
  def dishId = column[Long]("dish_id")
  def tagId = column[Long]("tag_id")
  def * = (dishId, tagId)

  // Foreign keys
  def dishIdFk = foreignKey("tagl_dish_id_fk", dishId, Tables.Dishes)(_.id)
  def tagIdFk = foreignKey("tagl_tag_id_fk", tagId, Tables.Tags)(_.id)

  // Composite primary keys
  def pk = primaryKey("tag_link_pk", (dishId, tagId))
}

class DTags(tag: Tag) extends Table[DTag](tag, "tags") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name")
  def * = (id.?, name) <> (DTag.tupled, DTag.unapply)
}



