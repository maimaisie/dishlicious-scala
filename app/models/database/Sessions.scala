package models.database

import java.sql.Date

import models.DSession

//import scala.slick.driver.MySQLDriver.simple._
import play.api.db.slick.Config.driver.simple._

/**
 * Created by maiwang on 2015-03-03.
 */

class Sessions(tag: Tag) extends Table[DSession](tag, "sessions") {
  def id = column[Long]("id", O.PrimaryKey)
  def createdAt = column[Date]("created_at")
  def * = (id, createdAt) <> (DSession.tupled, DSession.unapply)
}
