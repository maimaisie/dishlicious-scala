package models.database

import models.Dish
import play.api.db.slick.Config.driver.simple._

import scala.slick.ast.ColumnOption.DBType


/**
 * Created by maiwang on 2015-03-03.
 */
class Dishes(tag: Tag) extends Table[Dish](tag, "dishes") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name")
  def restaurant_id = column[Long]("restaurant")
  def price = column[Double]("price")
  def description = column[Option[String]]("description",DBType("Text"))
  def imagePath = column[Option[String]]("image_path")
  def * = (id.?, name, restaurant_id, price, description, imagePath) <> (Dish.tupled, Dish.unapply)

  def restaurantFk = foreignKey("d_restaurant_fk", restaurant_id, Tables.Restaurants)(_.id)
}

