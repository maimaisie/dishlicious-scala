package models.database

import com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException
import models.{Restaurant, Dish}
import play.api.db.slick.Config.driver.simple._

/**
 * Created by maiwang on 2015-01-27.
 */

object Tables {
  val Users = TableQuery[Users]
  val WhiteLists = TableQuery[WhiteLists]
  val BlackLists = TableQuery[BlackLists]

  val Tags = TableQuery[DTags]
  val TagLinks = TableQuery[TagLinks]

  val Sessions = TableQuery[Sessions]
  val SessionHistories = TableQuery[SessionHistories]

  val Restaurants = TableQuery[Restaurants]
  val Dishes = TableQuery[Dishes]

  val Comments = TableQuery[Comments]

}


object CreateDB {
  def main(args: Array[String]): Unit = {
    println("why are you not running")
    val manager = DatabaseManager
    try {

      println("success")
    } catch {
      case jdbc: MySQLSyntaxErrorException =>
        println(s"MySQL Syntax Error:\n${jdbc.getMessage}")
      case e: Exception =>
        println(s"Exception occured:\n")
        e.printStackTrace()
    }

    /*
    // Connect to the models.database and execute the following block within a session
    Database.forURL(url = "jdbc:mysql://localhost:3306/dishlicious_test",
                    driver = "com.mysql.jdbc.Driver",
                    user = "root",
                    password = "").withSession {
      implicit session =>

        // Create the tables, including primary and foreign keys
        (Tables.SessionHistories.ddl).create
        println("Created SessionHistories")
    }*/
  }
}
