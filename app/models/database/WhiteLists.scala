package models.database

//import scala.slick.driver.MySQLDriver.simple._

import models.WhiteListItem
import play.api.db.slick.Config.driver.simple._

/**
 * Created by maiwang on 2015-03-03.
 */


class WhiteLists(tag: Tag) extends Table[WhiteListItem](tag, "whitelists") {
  def id = column[Long]("wid", O.PrimaryKey, O.AutoInc)
  def userId = column[Long]("user_id")
  def tagId = column[Long]("tag_id")
  def * = (id.?, userId, tagId) <> (WhiteListItem.tupled, WhiteListItem.unapply)

  def userFk = foreignKey("wl_user_fk", userId, Tables.Users)(_.id)
  def tagFk = foreignKey("wl_tag_fk", tagId, Tables.Tags)(_.id)
}
