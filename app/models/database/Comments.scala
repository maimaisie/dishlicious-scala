package models.database

import java.sql.Date
//import scala.slick.driver.MySQLDriver.simple._
import play.api.db.slick.Config.driver.simple._

/**
 * Created by maiwang on 2015-03-03.
 */
case class Comment(id: Int,
                   comment: String,
                   dish_id: Long,
                   user_id: Long,
                   date: Date,
                   rating: Int)

class Comments(tag: Tag) extends Table[Comment](tag, "comments") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def comment = column[String]("description")
  def dish = column[Long]("dish_id")
  def user = column[Long]("user_id")
  def date = column[Date]("date")
  def rating = column[Int]("rating")
  def * = (id, comment, dish, user, date, rating) <> (Comment.tupled, Comment.unapply)

  def userFk = foreignKey("c_user_fk", user, Tables.Users)(_.id)
  def dishFk = foreignKey("c_dish_fk", dish, Tables.Dishes)(_.id)
}
