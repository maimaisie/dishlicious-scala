package models.database

//import scala.slick.driver.MySQLDriver.simple._

import models.User
import play.api.db.slick.Config.driver.simple._

/**
 * Created by maiwang on 2015-03-03.
 */

class Users(tag: Tag) extends Table[User](tag, "users") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def username = column[String]("username")
  def password = column[Option[String]]("password")
  def facebookId = column[Long]("facebook_id")

  def * = (id.?, username, password, facebookId) <>
    (User.tupled, User.unapply)
}
