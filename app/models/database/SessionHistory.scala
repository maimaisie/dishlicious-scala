package models.database

//import scala.slick.driver.MySQLDriver.simple._
import play.api.db.slick.Config.driver.simple._

/**
 * Created by maiwang on 2015-03-03.
 */
case class SessionHistory(sessionId: Long,
                          userId: Long)

class SessionHistories(tag: Tag) extends Table[SessionHistory](tag, "session_hist") {
  def sessionId = column[Long]("session_id")
  def userId = column[Long]("user_id")
  def * = (sessionId, userId) <> (SessionHistory.tupled, SessionHistory.unapply)

  // Foreign keys
  def sessionIdFk = foreignKey("sh_session_id_fk", sessionId, Tables.Sessions)(_.id)
  def userIdFk = foreignKey("sh_user_id_fk", userId, Tables.Users)(_.id)

  // Composite primary keys
  def pk = primaryKey("session_hist_pk", (sessionId, userId))
}