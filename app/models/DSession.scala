package models

import java.sql.Date

import models.database.DatabaseManager

/**
 * Created by maiwang on 2015-03-13.
 */
case class DSession(id: Long,
                   createdAt: Date) {
  def getMembers: List[String] = {
    DatabaseManager.getUsersFromSessionId(id).map(_.username)
  }
}
