package models

import models.database.{DatabaseManager}

/**
 * Created by maiwang on 2015-02-10.
 */
case class Dish(id: Option[Long] = None,
                   name: String,
                   restaurant: Long,
                   price: Double,
                   description: Option[String] = None,
                   imagePath: Option[String] = None) // link to S3 directory path
{
  def getTags: List[DTag] = {
    id match {
      case None =>
        println(s"Getting tags from dish $name with no id")
        List.empty[DTag]
      case Some(x) => DatabaseManager.getTagsFromDish(x)
    }
  }

  def addTag(tagId: Long): Unit = {
    id match {
      case None =>
        println(s"Adding tag $tagId to Dish $name with no id")
      case Some(x) =>
        DatabaseManager.addTagToDish(tagId, x)
    }
  }

  def removeTag(tagId: Long): Unit = {
    id match {
      case None =>
        println(s"Removing tag $tagId to Dish $name with no id")
      case Some(x) =>
        DatabaseManager.removeTagFromDish(tagId, x)
    }
  }
}
