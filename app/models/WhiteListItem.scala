package models

/**
 * Created by maiwang on 2015-03-08.
 */
case class WhiteListItem(id: Option[Long] = None,
                     userId: Long,
                     tagId: Long)