package models

/**
 * Created by nehasharma on 15-03-13.
 */
case class DTag(id: Option[Long] = None,
                   name: String)
