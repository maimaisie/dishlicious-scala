package recommendation

import scala.collection.mutable.{ListBuffer, LinkedHashMap}
import scala.math._
import models.Dish
import models.database.DatabaseManager

/**
 * Created by c8gao on 2/6/2015.
 */
//loc: (Double, Double) = (latitude, longitude), distance in meters
class RecommendationEngine(whiteListOfTagIDsC: List[List[Long]],
                           blackListOfTagIDsC: List[List[Long]],
                           userLocC: (Double, Double) = (43.472433, -80.538030),
                           distanceC: Double = 10000) {

  var whiteListOfTagIDs: List[List[Long]] = whiteListOfTagIDsC
  var blackListOfTagIDs: List[List[Long]] = blackListOfTagIDsC
  var userLoc: (Double, Double) = userLocC
  var distance: Double = distanceC

  def recommendDishes(sortByDistance: Boolean): LinkedHashMap[Long, List[Dish]] = {
    val mergedWhiteList: List[Long] = mergeList(whiteListOfTagIDs)
    val mergedBlackList: List[Long] = mergeList(blackListOfTagIDs)
    val whiteListOfDishIdsFromListOfTagIds = getListOfDishIdsFromListOfTagIds(mergedWhiteList, userLoc, distance)
    val blackListOfDishIdsFromListOfTagIds = getListOfDishIdsFromListOfTagIds(mergedBlackList, userLoc, distance)
    val recommendedListOfDishIds: List [(Long, Int)] = recommendListOfDishIds(whiteListOfDishIdsFromListOfTagIds, blackListOfDishIdsFromListOfTagIds)
    val recommendedListOfDishes: List[(Dish, Int)] = recommendListOfDishes(recommendedListOfDishIds)


    var finalRecommendedDishes: LinkedHashMap[Long, List[Dish]] = LinkedHashMap()
    if (!sortByDistance){
      finalRecommendedDishes=recommendRestaurantsWithDishes(recommendedListOfDishes)
    }else{
      finalRecommendedDishes=sortRestaurtnsByDistance(recommendRestaurantsWithDishes(recommendedListOfDishes), userLoc, distance)
    }
    finalRecommendedDishes
  }

  private def recommendRestaurantsWithDishes (listOfDishTuples: List[(Dish, Int)]): LinkedHashMap[Long, List[Dish]] = {
    var restaurantsToDishes: LinkedHashMap [Long, ListBuffer[(Dish, Int)]] = new LinkedHashMap [Long, ListBuffer[(Dish, Int)]]()
    for ( i <- listOfDishTuples){
      if (restaurantsToDishes.contains(i._1.restaurant)){
        restaurantsToDishes(i._1.restaurant) += ((i._1, i._2))
      }else{
        restaurantsToDishes += (i._1.restaurant -> ListBuffer((i._1, i._2)))
      }
    }


    var restaurantScoresList: ListBuffer[(Long, Int)] = ListBuffer()
    for (r <- restaurantsToDishes){
      var count: Int=0;
      for (l <- r._2){
        count += l._2
      }
      restaurantScoresList += ((r._1,count))
    }

    restaurantScoresList= restaurantScoresList.sortBy{_._2}.reverse
    var sortedRestaurantsToDishes: LinkedHashMap [Long, List[Dish]] = LinkedHashMap()
    restaurantScoresList.foreach( r=>
      sortedRestaurantsToDishes += ( r._1 -> restaurantsToDishes(r._1).toList.map(k => k._1))
    )
    sortedRestaurantsToDishes
  }

  private def sortRestaurtnsByDistance (restaurantsToDishes: LinkedHashMap[Long, List[Dish]], userLoc: (Double, Double), distance: Double): LinkedHashMap[Long, List[Dish]] = {
    val restIds: List[Long] = restaurantsToDishes.toList.map(k => k._1)
    val restLocs = getRestaurantsAndTheirLocationsFromRestaurantIds(restIds.toList)
    val listOfRestAndDistTuples: ListBuffer[(Long, Double)]= ListBuffer()
    for ((k,v) <- restLocs){
      val dist = getDistance(userLoc, v)
      listOfRestAndDistTuples+= ((k, dist))
    }
    val sortedRestaurtnsByDistance: List[Long]= listOfRestAndDistTuples.toList.sortBy{_._2}.map(x => x._1)
    val sortedRestaurantsToDishes: LinkedHashMap[Long, List[Dish]]= LinkedHashMap()
    sortedRestaurtnsByDistance.foreach(restId=>
      sortedRestaurantsToDishes += (restId -> restaurantsToDishes(restId))
    )
    sortedRestaurantsToDishes
  }

//  private def filterDishesOnDistance (listOfDishes: List[Dish], userLoc: (Double, Double), distance: Double): List[Dish] = {
//    val restIds = getRestaurantIdsFromListOfDishes(listOfDishes)
//    val restLocs = getRestaurantsAndTheirLocationsFromRestaurantIds(restIds.toList)
//    val filteredRests: ListBuffer[Long] = ListBuffer()
//    for ((k,v) <- restLocs){
//      val dist = getDistance(userLoc, v)
//      if (dist <= distance){
//        filteredRests += k
//      }
//    }
//
//    var listOfFilteredDishes: ListBuffer[Dish] = ListBuffer()
//    for (i <- listOfDishes){
//      if (filteredRests.contains(i.restaurant)){
//        listOfFilteredDishes += i
//      }
//    }
//    listOfFilteredDishes.toList
//  }
//
//  private def getRestaurantIdsFromListOfDishes(listOfDishes: List[Dish]): Set[Long] = {
//    var listOfRestaurantIds: Set[Long] = Set()
//    listOfDishes.foreach( dish =>
//      listOfRestaurantIds = listOfRestaurantIds ++ Set(dish.restaurant)
//    )
//    listOfRestaurantIds
//  }

  private def recommendListOfDishes (listOfDishIdCountTuples: List [(Long, Int)] ): List[(Dish, Int)] ={
    val listOfDishIds = listOfDishIdCountTuples.map(k => k._1)
    val dishesMap = getDishesFromDishIds(listOfDishIds)
    listOfDishIdCountTuples.map(x => (dishesMap(x._1), x._2))
  }

  private def recommendListOfDishIds( whiteListOfDishIdsFromListOfTagIds: List[Long], blackListOfDishIdsFromListOfTagIds: List[Long] ): List [(Long, Int)] = {
    val whitelistCounts: Map [Long, Int] = getIdFrequencyFromLists(whiteListOfDishIdsFromListOfTagIds)
    val blacklistCounts: Map [Long, Int] = getIdFrequencyFromLists(blackListOfDishIdsFromListOfTagIds)
    //eliminate the dishes that more or equal occurrence of the tag on blacklist than the occurrence of the tag on whitelist
    var mergedMap: Map [Long, Int] = Map()
    for ((k,v) <- whitelistCounts) {
      if (blacklistCounts.contains(k)){
        if (blacklistCounts(k) < v){
          mergedMap += (k -> (v-blacklistCounts(k)))
        }
      } else {
        mergedMap += (k -> v)
      }
    }

    //get sorted list from acceptableMap in order of number of people like it in whitelist
    val recommendListOfDishIds: List [(Long, Int)] =  mergedMap.toList
    recommendListOfDishIds
  }

  private def mergeList( listOfList: List[List[Long]] ): List[Long]= {
    var merge: List[Long] = List()
    for( i <- listOfList ){
      merge = merge::: i
    }
    merge
  }

  private def getIdFrequencyFromLists( listOfIds: List[Long] ): Map [Long, Int] = {
    listOfIds.groupBy(w => w).mapValues(_.size)
  }

  //call API to get distance
  private def getDistance(loc1: (Double, Double), loc2: (Double, Double)): Double ={
    DatabaseManager.getDistance(loc1, loc2)
  }

  //call API get list of Dish ids
  private def getListOfDishIdsFromListOfTagIds( listOfTagIds: List[Long], userLoc: (Double, Double), threshold: Double): List[Long] = {
    var listOfListOfDishIds = DatabaseManager.getDishesFromTags(listOfTagIds, userLoc, threshold)
    if (listOfListOfDishIds==null || listOfListOfDishIds.isEmpty){
      val defaultLoc = (43.472096, -80.537340)
      listOfListOfDishIds = DatabaseManager.getDishesFromTags(listOfTagIds, defaultLoc, threshold)
    }
    mergeList(listOfListOfDishIds)
  }

  //call API get list of Dishes
  private def getDishesFromDishIds(dishIds: List[Long]): Map[Long, Dish] ={
    val listOfDishes = DatabaseManager.getDishes(dishIds)
    listOfDishes.map(d => (d.id match{
      case Some(id) => id
      case None => throw DishNotFoundException("Error: cannot find dishid")
    }) -> d).toMap
  }

  //call API to get all restaurants' locations
  private def getRestaurantsAndTheirLocationsFromRestaurantIds(restaurantIds: List[Long]): Map[Long, (Double, Double)] = {
    DatabaseManager.getGeoCodes(restaurantIds)
  }
}

case class DishNotFoundException(msg: String) extends RuntimeException(msg: String)

case class RestaurantLocationNotFoundException(msg: String) extends RuntimeException(msg: String)