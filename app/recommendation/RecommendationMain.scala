package recommendation

import models.Dish

import scala.collection.mutable._

/**
 * Created by c8gao on 3/9/2015.
 */
object RecommendationMain {
  def main(args: Array[String]) {
    val whitelist1 :List[Long] = List(1, 4, 5, 6, 7, 8, 9, 10)
    val whitelist2 :List[Long] = List(9, 3, 2, 1, 10, 8, 9, 4)
    val whitelist3 :List[Long] = List(9, 3, 8)
    val whitelist4 :List[Long] = List(9, 10, 2, 3)
    val whitelists = List (whitelist1, whitelist2, whitelist3, whitelist4)
    //(1:2, 2:2, 3:3, 4:2, 5:1, 6:1, 7:1, 8:3, 9:4, 10:3)

    val balcklist1:List[Long] = List(2, 5)
    val blacklist2:List[Long] = List(6, 9)
    val blacklist3:List[Long] = List(5, 6, 7)
    val blacklist4:List[Long] = List(10, 2, 3,8,1)
    val blacklists = List (balcklist1, blacklist2, blacklist3, blacklist4)
    //(1:1, 2:2, 3:1, 4:0, 5:2, 6:2, 7:1, 8:1, 9:1, 10:1)
    //(1:1, 2:0, 3:2, 4:2, 5:-1, 6:-1, 7:-1, 8:2, 9:3, 10:2)

    val userLoc = (43.469840, -80.542420)
    val distance = 5000 //meters
    val recommend = new RecommendationEngine(whitelists, blacklists, userLoc, distance)
    val result : LinkedHashMap[Long, List[Dish]] = recommend.recommendDishes(false)
    println("Result: ")
    for ((k,v) <- result){
      print("Restaurant Id: "+k+" List:")
      for (i <- v){
        print(i.id+" ")
      }
    }
  }
}
