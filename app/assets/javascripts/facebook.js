
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    if (response.authResponse && response.status === "connected") {
        var user_id=response.authResponse.userID;
        setProfilePic(user_id);
    } else {
        jsRoutes.controllers.Application.logout().ajax({success: function(data){
          if (data === "true") {
            window.location = "/";
          }
        }});
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1452793288306821',
      cookie     : true,  // enable cookies to allow the server to access
                          // the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.2' // use version 2.2
    });

    // Now that we've initialized the JavaScript SDK, we call
    // FB.getLoginStatus().  This function gets the state of the
    // person visiting this page and can return one of three states to
    // the callback you provide.  They can be:
    //
    // 1. Logged into your app ('connected')
    // 2. Logged into Facebook, but not your app ('not_authorized')
    // 3. Not logged into Facebook and can't tell if they are logged into
    //    your app or not.
    //
    // These three cases are handled in the callback function.

    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  };

  function loginLogout() {
    if ($('#loginLogoutBtn').text()=='Login'){
      var login = {
        success: function(data){
          // if new user
          if(data === "true") {
            window.location.replace("/settings");
            console.log("added new user: ", data);
          } else {
          // if signed up already
            console.log("data", data);
            window.location = "/";
          }
        },
        error: function(data){console.log("error could not add user: ", data);}
      };

      FB.login(function(response) {
        if (response.authResponse) {
          console.log("connected, time to insert!", response.authResponse);
          var userId = response.authResponse.userID;
          setProfilePic(userId);
          getFacebookUserName(function(userName) {
            if(userName){
              jsRoutes.controllers.Application.saveUser(userId, userName)
                  .ajax(login);
            }
          });
        } else {
          console.log("User cancelled login or did not fully authorize.");
        }
      });
    }
    else{
      FB.logout(function(response) {
        var logout = { success: function(data) {location.reload(); console.log("User logged out");},
                       error: function(data) {console.log("error logging out: ",data);}
        };
        jsRoutes.controllers.Application.logout().ajax(logout);
      });
    }
  }
 

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

/** Helper functions **/

  function setProfilePic(user_id){
    var src="//graph.facebook.com/"+user_id+"/picture";
    $('#profile-picture').attr('src',src);
    $('#big-profile-pic').attr('src',src+"?type=large");
  }

  function getFacebookUserId(success){
    FB.api('/me', function(response) {
      var user_id = response? response.id : false;
      success(user_id);
    });
  }

  function getFacebookUserName(success){
    FB.api('/me', function(response) {
      var name = response? response.name : false;
      success(name);
    });
  }

  /*// Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    //console.log('Welcome!  Fetching your information.... ');
    var user_id;
    FB.api('/me', function(response) {
      //console.log('Successful login for: ' + response.name+" id: "+response.id);
      user_id=response.id;
      //console.log(JSON.stringify(response));

      $('#profile-picture').show();
      $('#profile-picture').attr('src',"//graph.facebook.com/"+user_id+"/picture");
    });

    FB.api('/me/permissions', function(response) {
      //console.log('Permissions granted: ');
      //console.log(JSON.stringify(response));
    });
    
    FB.api('/me/likes', function(response) {
      //console.log('Likes of the user: ');
      //console.log(JSON.stringify(response));
    });
  }*/

